<?php

namespace App\Http\Controllers;

use App\ShoppingItem;
use App\ShoppingList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShoppingItemController extends Controller
{
    public function index($list_id)
    {
        $list = ShoppingList::find($list_id);

        if (!empty($list)) {
            return response()->json($list->shoppingItems, 200);
        }
        return response()->json('No Shopping List found', 404);
    }

    public function show($list_id, $id)
    {
        $item = ShoppingItem::find($id);
        if (!empty($item) && $item->shopping_list_id == $list_id) {
            return response()->json($item, 200);
        }
        return response()->json('Item not found', 404);
    }

    public function save(Request $request, $list_id)
    {

        //$request = $this->parseRequest($request);

        $request['shopping_list_id'] = $list_id;

        DB::beginTransaction();
        try {
            $item = ShoppingItem::create($request->all());

            DB::commit();
            return response()->json($item, 201);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json("saving item failed: " . $e->getMessage(), 420);
        }
        var_dump($request);
        die();
    }

    public function update(Request $request, $list_id, $id)
    {
        DB::beginTransaction();
        try {
            $item = ShoppingItem::find($id);
            if (!empty($item) && $item->shopping_list_id == $list_id) {
                $item->update($request->all());
                $item->save();
                DB::commit();
                return response()->json($item, 201);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json("update item failed: " . $e->getMessage(), 420);
        }
    }

    public function delete($list_id, $id)
    {
        $item = ShoppingItem::find($id);
        if (!empty($item) && $item->shopping_list_id == $list_id) {
            $item->delete();
            return response()->json('item('.$id.') succesfullly deleted', 200);
        }
        return response()->json('Item not found', 404);
    }
}
