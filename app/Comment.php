<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    protected $fillable = ['content', 'shopping_list_id', 'user_id'];

    public function shoppingList() : BelongsTo {
        return $this->belongsTo(ShoppingList::class);
    }
}
