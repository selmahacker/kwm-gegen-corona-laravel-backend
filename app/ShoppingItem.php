<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ShoppingItem extends Model
{
    protected $fillable = ['description', 'amount', 'max_cost', 'shopping_list_id'];

    public function shoppingList() : BelongsTo {
        return $this->belongsTo(ShoppingList::class);
    }
}
