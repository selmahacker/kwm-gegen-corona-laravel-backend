<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoppingListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('fullfillment_date');
            $table->decimal('paid_price', 5, 2)->nullable();

            // user id from owner and volunteer
            $table->integer('owner_user_id')->unsigned();
            $table->integer('volunteer_user_id')->unsigned()->nullable();


            $state = ['offen', 'uebernommen', 'zugestellt'];
            $table->enum('state', $state)->default('offen');
        });

        Schema::table('shopping_lists', function (Blueprint $table) {
            // determine foreign keys
            $table->foreign('owner_user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('volunteer_user_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_lists');
    }
}
