<?php

namespace App\Http\Controllers;

use App\ShoppingList;
use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function index($list_id) {
        $list = ShoppingList::find($list_id);

        if (!empty($list)) {
            return response()->json($list->comments, 200);
        }
        return response()->json('No Shopping List found', 404);
    }
    public function show($list_id, $id){
        $comment = Comment::find($id);
        if (!empty($comment) && $comment->shopping_list_id == $list_id) {
            return response()->json($comment, 200);
        }
        return response()->json('Comment not found', 404);
    }
    public function save(Request $request, $list_id){

        //$request = $this->parseRequest($request);

        $request['shopping_list_id'] = $list_id;

        DB::beginTransaction();
        try {
            $comment = Comment::create($request->all());

            DB::commit();
            return response()->json($comment, 201);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json("saving comment failed: " . $e->getMessage(), 420);
        }
        var_dump($request);
        die();
    }
    public function update(Request $request, $list_id, $id){
        DB::beginTransaction();
        try {
            $comment = Comment::find($id);
            if (!empty($comment) && $comment->shopping_list_id == $list_id) {
                $comment->update($request->all());
                $comment->save();
                DB::commit();
                return response()->json($comment, 201);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json("update comment failed: " . $e->getMessage(), 420);
        }
    }
    public function delete($list_id, $id){
        $comment = Comment::find($id);
        if (!empty($comment) && $comment->shopping_list_id == $list_id) {
            $comment->delete();
            return response()->json('Comment('.$id.') succesfullly deleted', 200);
        }
        return response()->json('Comment not found', 404);
    }
}
