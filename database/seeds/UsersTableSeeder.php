<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Selma',
            'last_name' => 'Hacker',
            'street' => 'Finkstraße 4',
            'post_code' => '4040',
            'city' => 'Linz',
            'email' => 'selma@test.at',
            'password' => '1234',
            'role' => true
        ]);
        DB::table('users')->insert([
            'first_name' => 'Jonas',
            'last_name' => 'Schmides',
            'street' => 'Steingasse 20',
            'post_code' => '4020',
            'city' => 'Linz',
            'email' => 'jonas@test.at',
            'password' => '4321',
            'role' => false
        ]);
    }
}
