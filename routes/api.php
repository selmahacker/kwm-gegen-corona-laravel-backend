<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['api', 'cors']], function () {
    Route::post('auth/login', 'Auth\ApiAuthController@login');

    //Route::post('auth/login', 'Auth\ApiAuthController@login');
});

Route::group(['middleware' => ['api', 'cors', 'jwt.auth']], function () {
    Route::put('shopping_lists/{id}', 'ShoppingListController@update');
    Route::delete('shopping_lists/{id}', 'ShoppingListController@delete');
    Route::post('shopping_lists', 'ShoppingListController@save');

    Route::post('shopping_lists/{list_id}/shopping_items', 'ShoppingItemController@save');
    Route::put('shopping_lists/{list_id}/shopping_items/{id}', 'ShoppingItemController@update');
    Route::delete('shopping_lists/{list_id}/shopping_items/{id}', 'ShoppingItemController@delete');

    Route::post('shopping_list/{list_id}/comments', 'CommentController@save');
    Route::put('shopping_list/{list_id}/comments/{id}', 'CommentController@update');
    Route::delete('shopping_list/{list_id}/comments/{id}', 'CommentController@delete');
});


// shopping LIST
Route::get('shopping_lists', 'ShoppingListController@index');
Route::get('shopping_lists/{id}', 'ShoppingListController@show');


// shopping list ITEM
Route::get('shopping_lists/{list_id}/shopping_items', 'ShoppingItemController@index');
Route::get('shopping_lists/{list_id}/shopping_items/{id}', 'ShoppingItemController@show');


// shopping list COMMENTS
Route::get('shopping_list/{list_id}/comments', 'CommentController@index');
Route::get('shopping_list/{list_id}/comments/{id}', 'CommentController@show');



