<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ShoppingList extends Model
{
    protected $fillable = ['fullfillment_date', 'paid_price', 'owner_user_id', 'volunteer_user_id', 'state'];

    public function shoppingItems() : HasMany {
        return $this->hasMany(ShoppingItem::class);
    }

    public function comments() : HasMany {
        return $this->hasMany(Comment::class);
    }

    public function owner() : BelongsTo {
        return $this->belongsTo(User::class, 'owner_user_id');
    }

    public function volunteer() : BelongsTo {
        return $this->belongsTo(User::class, 'volunteer_user_id');
    }
}
