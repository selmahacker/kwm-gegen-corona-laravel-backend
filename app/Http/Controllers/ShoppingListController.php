<?php

namespace App\Http\Controllers;

use App\ShoppingList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShoppingListController extends Controller
{
    public function index()
    {
        $lists = ShoppingList::with('owner')
            ->with('volunteer')->with('shoppingItems')->with('comments')->get();
        return $lists;
    }

    public function show($id)
    {
        $list = ShoppingList::with('shoppingItems')
            ->with('owner')
            ->with('volunteer')
            ->with('comments')
            ->find($id);

        //return $list;
        if(!empty($list)) {
//            $list['items'] = $list->shoppingItems();
//            $list['owner'] = $list->owner();
//            $list['volunteer'] = $list->volunteer();
            return response()->json($list, 200);
        }
        return response()->json('No Shopping List found', 404);
    }

    public function save(Request $request)
    {
        //$request = $this->parseRequest($request);

        DB::beginTransaction();
        try {
            $list = ShoppingList::create($request->all());


            DB::commit();
            return response()->json($list, 201);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json("saving list failed: " . $e->getMessage(), 420);
        }
        var_dump($request);
        die();

    }

    public function update(Request $request, $id) {
        DB::beginTransaction();
        try {
            $list = ShoppingList::find($id);
            if(!empty($list)) {
                $request = $this->parseRequest($request);
                $list->update($request->all());
                $list->save();
                DB::commit();
                return response()->json($list, 201);
            }
        } catch (\Exception $e) {
            DB::rollback();
        }
        var_dump($request);
        die();
    }

    public function delete($id) {
        $list = ShoppingList::find($id);
        if(!empty($list)) {
            $list->delete();
            return response()->json('list('.$id.') succesfullly deleted', 200);
        }
        return response()->json('Shopping List not found', 404);
    }

    // HILFSMETHODE für save() => convert json-date in object-date
    private function parseRequest(Request $request): Request
    {
        $date = new \DateTime($request->fullfillment_date);
        $request['published'] = $date;
        return $request;
    }


}
